import { cond, log, splitArr } from './lib'
import dirTree, { DirectoryTree } from 'directory-tree'
import { generateRegexRule, generateStaticRule } from './templates'

import filter from 'lodash/fp/filter'
import flatten from 'lodash/fp/flatten'
import flow from 'lodash/fp/flow'
import fs from 'fs'
import { renderTemplateFile } from 'template-file'
import update from 'lodash/fp/update'

const FILES = 0
const DIRS = 1
const STAT_FILES = 0
const DYN_FILES = 1
const STAT_DIRS = 2
const DYN_DIRS = 3
const REGEX_NAME = true
const STAT_NAME = false

export interface Context {
  tree: DirectoryTree[]
  units?: DirectoryTree[][]
  isPathDynamic: boolean
  dynamic?: boolean
  root: string
  path: string
  configs: string[]
}

export interface FileDescriptor {
  root: string
  name: string
  matchName: string
  absolutePath: string
  label: string
  path: string
}

const createFileDescriptor = (ctx: Context) => (
  file: DirectoryTree,
): FileDescriptor => ({
  root: ctx.root,
  name: file.name.slice(0, -5),
  matchName: isDynamicName(file.name) ? '[^/]+' : file.name.slice(0, -5),
  absolutePath: file.path.replace(`/${file.name}`, '').replace(/^[^/]+/, ''),
  label: file.path.slice(0, -5).replace(/\//g, '_').replace(/[[\]]/g, '_'),
  path: ctx.path.replace(/^[^/]+/, ''),
})

const filterNext = filter(({ name }: DirectoryTree) => name !== '_next')
const filter404 = filter(({ name }: DirectoryTree) => name !== '404.html')

const splitToFilesAndDirs = splitArr(
  ({ type }: DirectoryTree) => type === 'file',
)

const splitToStatAndDyns = splitArr(
  ({ name }: DirectoryTree) => !isDynamicName(name),
)

const isDynamicName = (name: string) => /^\[.+\](\.html)?$/.test(name)

const isCtxDynamic = ({ dynamic, isPathDynamic }: Context): boolean =>
  dynamic || isPathDynamic

const isCtxPathDynamic = ({ isPathDynamic }: Context): boolean => isPathDynamic

const filesToConfigs = (unit: number, isRegexName: boolean) => (
  ctx: Context,
): Context => (
  ctx.configs.push(
    ...ctx.units[unit]
      .map(createFileDescriptor(ctx))
      .map(isRegexName ? generateRegexRule : generateStaticRule),
  ),
  ctx
)

const emptyUnit = (unit: number) => (ctx: Context): Context => (
  (ctx.units[unit] = []), ctx
)

const statDirsToConfigs = (ctx: Context): Context => (
  ctx.configs.push(
    ...ctx.units[STAT_DIRS].map((dir) =>
      toConfig({
        ...ctx,
        tree: dir.children,
        path: `${ctx.path}/${dir.name}`,
        configs: [],
      }),
    ).flat(),
  ),
  ctx
)

const dynDirsToConfigs = (ctx: Context): Context => (
  ctx.configs.push(
    ...ctx.units[DYN_DIRS].map((dir) =>
      toConfig({
        ...ctx,
        tree: dir.children,
        path: `${ctx.path}/[^/]+`,
        isPathDynamic: true,
        configs: [],
      }),
    ).flat(),
  ),
  ctx
)

const splitToUnits = flow(
  filterNext,
  filter404,
  splitToFilesAndDirs,
  update(FILES, splitToStatAndDyns) as (
    tree: DirectoryTree[][],
  ) => DirectoryTree[][][],
  update(DIRS, splitToStatAndDyns) as (
    tree: DirectoryTree[][][],
  ) => DirectoryTree[][][],
  flatten,
)

const defineUnits = (ctx: Context): Context => (
  (ctx.units = splitToUnits(ctx.tree)), ctx
)

const defineDynamic = (ctx: Context): Context => (
  (ctx.dynamic =
    ctx.isPathDynamic ||
    ctx.units[DYN_FILES].length > 0 ||
    ctx.units[DYN_DIRS].length > 0),
  ctx
)

const prepareContext = flow(defineUnits, defineDynamic)

const toString = ({ configs }: Context): string => configs.join('\n\n')

const toConfig = flow(
  prepareContext,
  cond(
    isCtxDynamic,
    cond(
      isCtxPathDynamic,
      filesToConfigs(STAT_FILES, REGEX_NAME),
      filesToConfigs(STAT_FILES, STAT_NAME),
    ),
    emptyUnit(STAT_FILES),
  ),
  filesToConfigs(DYN_FILES, REGEX_NAME),
  statDirsToConfigs,
  dynDirsToConfigs,
  toString,
)

renderTemplateFile('./nginx-config-gen/default.conf', {
  rules: toConfig({
    tree: dirTree('./out', { extensions: /\.html$/, normalizePath: true })
      .children,
    path: '',
    root: '/usr/share/nginx/html/next-frontend',
    isPathDynamic: false,
    configs: [],
  }),
}).then((config) => {
  fs.writeFile('./conf/conf.d/default.conf', config, (err) => {
    if (err) {
      return console.error(err)
    }

    console.log('Done')
  })
})
