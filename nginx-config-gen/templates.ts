import { FileDescriptor } from './index'
import packageFile from '../package.json'

export const generateStaticRule = ({
  path,
  matchName,
  absolutePath,
  name,
}: FileDescriptor): string => `
  location = ${path}/${matchName} {
    try_files ${absolutePath}/${name}.html =404;
  }
`

export const generateRegexRule = ({
  root,
  path,
  matchName,
  absolutePath,
  name,
}: FileDescriptor): string => `
  location ~ ^(${path}/${matchName}$) {
    alias ${root}${absolutePath}/${name}.html;
  }
`
