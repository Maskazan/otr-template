import get from 'lodash/fp/get'
import identity from 'lodash/fp/identity'
import reduce from 'lodash/fp/reduce'
import util from 'util'

export const log = (label: string, path?: string) => <T>(arg: T): T => (
  console.log(
    `${label}:`,
    util.inspect(path === undefined ? arg : get(path)(arg), { depth: 5 }),
  ),
  arg
)

export const splitArr = <T>(
  splitter: (elem: T) => boolean,
): ((arr: T[]) => T[][]) =>
  reduce<T, T[][]>(
    ([A, B], elem) => (splitter(elem) ? [[...A, elem], B] : [A, [...B, elem]]),
    [[], []],
  )

export const cond = <T>(
  condition: (arg: T) => boolean,
  ifTrueCb: (arg: T) => T,
  ifFalseCb = identity as (arg: T) => T,
) => (arg: T): T => (condition(arg) ? ifTrueCb(arg) : ifFalseCb(arg))
