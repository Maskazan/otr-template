import codegen from '../src/swagger/codegen'
import fs from 'fs'
import path from 'path'

export default async (): Promise<void> => {
  if (
    !fs.existsSync(path.resolve('./lib/use-api/__tests__/swagger/index.ts'))
  ) {
    return codegen({
      methodName: 'test',
      // openApi: 'v3',
      remoteUrl: 'https://petstore.swagger.io/v2/swagger.json',
      outputDir: './lib/use-api/__tests__/swagger',
    })
  }
}
