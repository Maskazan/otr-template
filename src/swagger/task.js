/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */

const codegen = require('./codegen')
const urlJoin = require('url-join')
const dotenv = require('dotenv')

dotenv.config()

codegen({
  methodName: 'otr',
  remoteUrl: urlJoin(
    process.env.NEXT_PUBLIC_API_URL,
    process.env.NEXT_PUBLIC_API_PATH,
    process.env.NEXT_PUBLIC_SWAGGER_JSON_NAME,
  ),
  outputDir: './src/swagger',
})
