/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */

const { codegen } = require('swagger-axios-codegen')
const replace = require('replace-in-file')
const path = require('path')

module.exports = async (options) => {
  const outFile = path.resolve(options.outputDir, 'index.ts')

  await codegen({
    strictNullChecks: false,
    modelMode: 'interface',
    ...options,
  })

  await replace({
    files: outFile,
    from: /{ AxiosInstance } from 'axios'/,
    to: "{ AxiosInstance, CancelToken } from 'axios'",
  })

  await replace({
    files: outFile,
    from: /interface IRequestConfig {/,
    to: 'interface IRequestConfig {\n  cancelToken?: CancelToken,',
  })
}
