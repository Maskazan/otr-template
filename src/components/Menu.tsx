import Link from 'next/link'

const Menu = (): JSX.Element => {
  return (
    <ul>
      <li>
        <Link href="/">
          <a>Main</a>
        </Link>
      </li>
      <li>
        <Link href="/test-page">
          <a>Test</a>
        </Link>
      </li>
      <li>
        <Link href="/posts/about">
          <a>About posts</a>
        </Link>
      </li>
    </ul>
  )
}

export default Menu
