# 🌐 useMI(value)

Возвращает функцию, которая мемоизирует переданное значение. Полезен при передачи объектов в `deps` в `useEffect`, `useMemo` и т.д. Создан для замены [use-deep-compare-effect](https://github.com/kentcdodds/use-deep-compare-effect)

## Полная сигнатура
**useMI(): Identity**

## Пример

```JSX
import useMI from '@lib/use-memoized-identity'
import { useEffect } from 'react'

const Component = ({ obj }) => {
  const mi = useMI()

  obj = mi(obj)

  useEffect(() => {
    //...
  }, [obj])

  return (
    // JSX
  )
} 
```