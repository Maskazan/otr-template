import memoize from 'fast-memoize'
import { useMemo } from 'react'

const identity = <T>(value: T): T => value

export type Identity = typeof identity

const cb = (): Identity => memoize(identity)
const deps = []

const useMI = (): Identity => {
  return useMemo(cb, deps)
}

export default useMI
