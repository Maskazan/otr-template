import { EnumPetStatus, Pet, PetService, serviceOptions } from './swagger'

import axios from 'axios'
import axiosMockAdapter from 'axios-mock-adapter'
import { renderHook } from '@testing-library/react-hooks'
import useAPI from '../index'

const axiosMock = new axiosMockAdapter(axios)
serviceOptions.axios = axios

afterEach(() => {
  axiosMock.reset()
})

const getPetStub = (id: number): Pet => ({
  category: {
    id: 1,
    name: 'Golden',
  },
  id,
  name: 'Fred',
  photoUrls: [],
  status: EnumPetStatus.available,
  tags: [
    {
      id: 1,
      name: 'cao',
    },
  ],
})

describe('useAPI', () => {
  it('should loading', async () => {
    const { result, waitForNextUpdate } = renderHook(() =>
      useAPI(PetService.getPetById, { petId: 0 }),
    )

    expect(result.current.loading).toBeTruthy()
    await waitForNextUpdate()
    expect(result.current.loading).toBeFalsy()
  })

  it('should response data', async () => {
    axiosMock.onGet('/v2/pet/2').replyOnce(200, getPetStub(2))
    const { result, waitForNextUpdate } = renderHook(() =>
      useAPI(PetService.getPetById, { petId: 2 }),
    )

    expect(result.current.data).toBeUndefined()
    await waitForNextUpdate()
    expect(result.current.data).toEqual(getPetStub(2))
  })

  it('should response error', async () => {
    axiosMock.onGet('/v2/pet/2').replyOnce(404)
    const { result, waitForNextUpdate } = renderHook(() =>
      useAPI(PetService.getPetById, { petId: 2 }),
    )

    expect(result.current.error).toBeUndefined()
    await waitForNextUpdate()
    expect(result.current.error).toBeDefined()
  })

  it('should be canceled', async () => {
    axiosMock
      .onGet('/v2/pet/3')
      .replyOnce(200, getPetStub(3))
      .onGet('/v2/pet/2')
      .replyOnce(200, getPetStub(2))

    const { result, waitForNextUpdate, rerender } = renderHook(
      ({ id }: { id: number }) => useAPI(PetService.getPetById, { petId: id }),
      { initialProps: { id: 3 } },
    )

    expect(result.current.data).toBeUndefined()
    expect(result.current.error).toBeUndefined()

    rerender({ id: 2 })
    await waitForNextUpdate()
    expect(result.current.error).toBe('canceled')
    expect(result.current.data).toEqual(getPetStub(2))
  })
})
