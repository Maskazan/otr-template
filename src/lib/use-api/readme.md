# 🌐 useAPI(apiMethod, params, safeParams?)

Адаптер swagger-codegen для ассинхронных запросов к API

## Полная сигнатура
**useAPI<T, P>(apiMethod: (params: P, options?: IRequestConfig) => Promise<T>, params: P, safeParams = true): APIResult<T>**

## Параметры
* **apiMethod** - сгенерированный метод для запросов к API
* **params** - параметры запроса
* **safeParams?** *(по умолчанию: true)* - включает мемоизацию параметров, дабы избежать перерендер при грязной передаче объекта. Рекомендуется вручную отключать флаг, чтобы увеличить производительность

## Возвращает
```javascript
{
  loading: boolean, // Состояние загрузки
  data: T | undefined, // Данные из ответа (Возвращается распаршенный json)
  error: AxiosError | undefined, // Полный объект ошибки (Данные ошибки - error.response.data)
}
```

## Пример

```JSX
  import useAPI from '@lib/use-api'
  import { PetService } from '@swagger'
  import { useMemo } from 'react

  const SafePet = ({ id }) => {
    const {
      loading,
      data,
      error
    } = useAPI(PetService.getPetById, { id })

    return (
      <p>
        {loading && 'Loading...'}
        {data && data.name}
        {error && JSON.stringify(error.response.data)}
      </p>
    )
  }

  // with safeParams = false
  const FastPet = ({ id }) => {
    const params = use(() => ({ id }), id)

    const {
      loading,
      data,
      error
    } = useAPI(PetService.getPetById, params, false)

    return (
      <p>
        {loading && 'Loading...'}
        {data && data.name}
        {error && JSON.stringify(error.response.data)}
      </p>
    )
  }
```