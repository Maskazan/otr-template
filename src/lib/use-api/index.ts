import {
  PayloadAction,
  Reducer,
  createAction,
  createReducer,
} from '@reduxjs/toolkit'
import axios, { AxiosError, CancelTokenSource } from 'axios'
import { useEffect, useReducer, useRef } from 'react'

import { IRequestConfig } from '@swagger'
import useMI from '@lib/use-memoized-identity'

interface APIResult<T> {
  loading: boolean
  error: AxiosError | undefined
  data: T | undefined
}

const setLoading = createAction<boolean, 'loading'>('loading')
const setError = createAction<unknown, 'error'>('error')
const setData = createAction<unknown, 'data'>('data')

type APIResultProps = 'loading' | 'error' | 'data'

const createAPIResultSetter = <T, P extends APIResultProps>(propName: P) => (
  state: APIResult<T>,
  action: PayloadAction<APIResult<T>[P], P>,
) => ({
  ...state,
  [propName]: action.payload,
})

const initialState: APIResult<unknown> = {
  loading: false,
  error: undefined,
  data: undefined,
}

const apiResultReducer = createReducer<APIResult<unknown>>(
  initialState,
  (builder) =>
    builder
      .addCase(setLoading, createAPIResultSetter<boolean, 'loading'>('loading'))
      .addCase(setError, createAPIResultSetter<unknown, 'error'>('error'))
      .addCase(setData, createAPIResultSetter<unknown, 'data'>('data')),
)

const useAPI = <T, P>(
  apiMethod: (params: P, options?: IRequestConfig) => Promise<T>,
  params: P,
  safeParams = true,
): APIResult<T> => {
  const mi = useMI()

  if (safeParams) {
    params = mi(params)
  }

  const [apiResult, dispatch] = useReducer(
    apiResultReducer as Reducer<APIResult<T>>,
    initialState as APIResult<T>,
  )

  const cancelToken = useRef<CancelTokenSource>(null)

  useEffect(() => {
    ;(async () => {
      try {
        dispatch(setLoading(true))
        dispatch(setError(undefined))

        if (cancelToken.current !== null) {
          cancelToken.current.cancel()
        }

        cancelToken.current = axios.CancelToken.source()
        const data = await apiMethod(params, {
          cancelToken: cancelToken.current.token,
        })

        dispatch(setData(data))
        dispatch(setLoading(false))
      } catch (err) {
        if (!axios.isCancel(err)) {
          dispatch(setError(err))
          dispatch(setLoading(false))
        } else if (process.env.NODE_ENV === 'test') {
          dispatch(setError('canceled'))
        }
      }
    })()
  }, [apiMethod, params])

  return apiResult
}

export default useAPI
