import * as Counter from './actions'

import {
  ActionCreatorWithPayload,
  ActionCreatorWithoutPayload,
} from '@reduxjs/toolkit'
import { useDispatch, useSelector } from 'react-redux'

import { RootState } from 'src/store'
import { useCallback } from 'react'

export const useCounterSelector = (): number => {
  const value = useSelector((state: RootState) => {
    return state.counter
  })

  return value.x
}

export type ActionCreators = [
  ActionCreatorWithoutPayload,
  ActionCreatorWithoutPayload,
  ActionCreatorWithPayload<number>,
]

export const useCounterActions = (): ActionCreators => {
  const dispatch = useDispatch()

  const increment = useCallback(() => dispatch(Counter.increment()), [])
  const decrement = useCallback(() => dispatch(Counter.decrement()), [])

  const setValue = useCallback(
    (value: number) => dispatch(Counter.setValue(value)),
    [],
  )

  return [
    increment as ActionCreatorWithoutPayload,
    decrement as ActionCreatorWithoutPayload,
    setValue as ActionCreatorWithPayload<number>,
  ]
}
