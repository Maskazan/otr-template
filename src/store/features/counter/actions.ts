import { createAction } from '@reduxjs/toolkit'

export const increment = createAction<void, 'increment'>('increment')
export const decrement = createAction<void, 'decrement'>('decrement')
export const setValue = createAction<number, 'set-value'>('set-value')
