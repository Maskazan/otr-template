import { PayloadAction, createReducer } from '@reduxjs/toolkit'
import { decrement, increment, setValue } from './actions'

const initialState = { x: 0 }
type State = Readonly<typeof initialState>

export default createReducer(initialState, (builder) =>
  builder
    .addCase(increment, (state) => ({ x: state.x + 1 }))
    .addCase(decrement, (state) => ({ x: state.x - 1 }))
    .addCase(setValue, (state, action: PayloadAction<number>) => ({
      x: action.payload,
    })),
)

export * from './actions'
export * from './hooks'
