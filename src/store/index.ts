import { configureStore } from '@reduxjs/toolkit'
import counter from './features/counter'

const store = configureStore({
  reducer: {
    counter,
  },
  devTools: process.env.NODE_ENV === 'development',
})

export default store
export type RootState = ReturnType<typeof store.getState>
