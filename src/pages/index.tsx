// import { Button } from 'antd'
import Head from 'next/head'
import Menu from 'src/components/Menu'
import { PetService } from '@swagger'
import useAPI from '@lib/use-api'

export default function Home(): JSX.Element {
  const { data, loading, error } = useAPI(PetService.getPetById, { petId: 1 })
  console.log('finished', data, loading, error)

  return (
    <div className="container">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <h1>Main Page</h1>
      <p>
        {loading && 'Loading...'}
        {!loading && !error && <b>{JSON.stringify(data)}</b>}
        {error && JSON.stringify(error.response.data)}
      </p>
      <Menu />
    </div>
  )
}
