import { useRouter } from 'next/router'

const Post = (): JSX.Element => {
  const router = useRouter()
  const { id } = router.query

  return <h1>AC {id}!</h1>
}

export default Post
