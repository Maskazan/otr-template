import { useRouter } from 'next/router'

const Post = (): JSX.Element => {
  const router = useRouter()
  const { id, name } = router.query

  return (
    <h1>
      A {name} {id}!
    </h1>
  )
}

export default Post
