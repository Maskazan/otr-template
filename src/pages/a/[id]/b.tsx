import { useRouter } from 'next/router'

const Post = (): JSX.Element => {
  const router = useRouter()
  const { id } = router.query

  return <h1>AB {id}!</h1>
}

export default Post
