import { AppProps } from 'next/app'
import { Provider } from 'react-redux'
import axiosInstance from '../axios-instance'
import { serviceOptions } from '@swagger'
import store from '@store'

serviceOptions.axios = axiosInstance

const App = ({ Component, pageProps }: AppProps): JSX.Element => {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  )
}

export default App
