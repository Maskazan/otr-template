import { useCounterActions, useCounterSelector } from '@store/features/counter'

import { Button } from 'antd'
import Head from 'next/head'
import Link from 'next/link'
import Menu from 'src/components/Menu'
import { PetService } from 'src/swagger'
import useAPI from 'src/lib/use-api'
import { useMemo } from 'react'

export default function Home(): JSX.Element {
  const count = useCounterSelector()
  const [inc, dec] = useCounterActions()
  const petParams = useMemo(() => ({ petId: count }), [count])
  // const { loading, error, data } = useAPI(PetService.getPetById, petParams)
  return (
    <div className="container">
      <Head>
        <title>Test</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <h1>Test Page</h1>
      <span>Count: {count}</span>
      <br />
      <Button type="primary" onClick={dec}>
        -
      </Button>
      <Button type="primary" onClick={inc}>
        +
      </Button>
      <br />
      <Link
        href={{ pathname: 'posts/[id]' }}
        as={{ pathname: 'posts/' + count }}
      >
        <a>Go to post {count}</a>
      </Link>
      <br />
      <Menu />
    </div>
  )
}
