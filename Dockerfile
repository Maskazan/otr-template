# build environment
FROM node:12.18.0-alpine3.11 as builder
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY package.json /usr/src/app/package.json
RUN npm install --silent
COPY . /usr/src/app
RUN npm run build

# production environment
FROM nginx:1.18.0-alpine
RUN rm -rf /etc/nginx/conf.d
COPY --from=builder /usr/src/app/conf /etc/nginx
COPY --from=builder /usr/src/app/out /usr/share/nginx/html/next-frontend

# Copy .env file and shell script to container
WORKDIR /usr/share/nginx/html/next-frontend
COPY .env .

# Add bash
RUN apk add --no-cache bash

EXPOSE 80

# Start Nginx server
CMD ["/bin/bash", "-c", "nginx -g \"daemon off;\""]
