# Название проекта

## 💻 Требования к окружению
* Node.js 12+
* Docker
* Chrome 41
* Git

## 🏗️ Сборка

```bash
# Development
npm run dev

# Production
npm run build
```

### Особенности сборки
* Директория сборки `/out`
* Настройка Webpack & Next.js - `next.config.js`
* Перед сборкой скачивается **swagger config**, настройка ресурса в `/src/swagger/task.js`
* После сборки генерируется **nginx config**. Базовый шаблон — `/nginx-config-gen/default.conf`. Шаблоны генератра — `/nginx-config-gen/templates.ts`

## ⚙️ Стек технологий
* [TypeScript](https://www.typescriptlang.org/docs/home.html)
* [React](https://ru.reactjs.org/docs/getting-started.html)
* [Redux Toolkit](https://redux-toolkit.js.org/api/configureStore)
* [Next.js](https://nextjs.org/docs/getting-started)
* [antd@v3](https://3x.ant.design/docs/react/introduce)
* [LESS](http://lesscss.org/usage/)
* [Webpack](https://webpack.js.org/concepts/)
* [Babel](https://babeljs.io/docs/en/)
* [ESLint](https://eslint.org/docs/user-guide/getting-started)
* [Prettier](https://prettier.io/docs/en/install.html)
* [Jest](https://jestjs.io/docs/en/getting-started)

## 🧪 Автотесты

```bash
npm run test
```

### Особенности автотестов
* Настроены хуки на `git push` & `git pull` через [husky](https://github.com/typicode/husky).
* В директории `/jest` настраивается глобальное окружение

## 📋 Псевдонимы
* **@components/*** - /src/components/*
* **@swagger** - /src/swagger/index
* **@swagger/*** - /src/swagger/*
* **@store** - /src/store/index
* **@store/*** - /src/store/*
* **@lib/*** - /src/lib/*

## 🗄️ Правила ведения стора
* Логика внутри reducer'ов не выше уровня CRUD, логика должна быть заключена в action creator'ах, при этом, по возможности, максимально вытеснена в selector'ы
* Ветки стора разбиваются в директории `@store/features/*` и объединяются в `@store/index`
* Структура веток:
  * **/actions.ts** - синхронные и ассинхронные action'ы
  * **/selectors.ts** - селекторы
  * **/hooks.ts** - фасадные хуки для работы с данными ветки
  * **/index.ts** - реализация `reducer'a` и реэкспорт остальных сущностей
  * **/__tests__** - автотесты

## 📚 Внутренние библиотеки `@lib`

* [use-api](/src/lib/use-api/readme.md) - адаптер под сгенерированные методы api
* [use-memoized-identity](/src/lib/use-memoized-identity/readme.md) - мемоизация значений
* [use-previous](/src/lib/use-previous/readme.md) - возвращает предыдуще значение

### Правила ведения
* Если какая-то логика переиспользуется внутри проекта и не является компонентом, должна быть вынесена в папку `@lib`
* Структура библиотек:
  * **/index.ts** - реализация библиотеки
  * **/reabme.md** - документация
  * **/__tests__** - автотесты
